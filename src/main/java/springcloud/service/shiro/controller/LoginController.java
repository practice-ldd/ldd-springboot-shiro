package springcloud.service.shiro.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springcloud.service.shiro.model.ResultMap;
import springcloud.service.shiro.mapper.UserMapper;

/**
 * @Auther: liudong
 * @Date: 18-11-2 15:05
 * @Description:
 */
@Slf4j
@RestController
public class LoginController {
    private final ResultMap resultMap;
    private final UserMapper userMapper;

    @Autowired
    public LoginController(ResultMap resultMap, UserMapper userMapper) {
        this.resultMap = resultMap;
        this.userMapper = userMapper;
    }

    @RequestMapping(value = "/notLogin", method = RequestMethod.GET)
    public ResultMap notLogin() {
        return resultMap.success().message("您尚未登陆！");
    }

    @RequestMapping(value = "/notRole", method = RequestMethod.GET)
    public ResultMap notRole() {
        return resultMap.success().message("您没有权限！");
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ResultMap logout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return resultMap.success().message("成功注销！");
    }

    @RequestMapping(value = "/api/v1", method = RequestMethod.GET)
    public ResultMap getMessage() {
        return resultMap.success().code(200).message("成功访问！");
    }

    /**
     * 登陆
     *
     * @param username 用户名
     * @param password 密码
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResultMap login(String username, String password) {
        // 从SecurityUtils里边创建一个 subject
        Subject subject = SecurityUtils.getSubject();
        // 在认证提交前准备 token（令牌）
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try {
            // 执行认证登陆
            subject.login(token);
            log.info("对用户[" + username + "]进行登录验证...验证通过");
            if (subject.isAuthenticated()) {
                log.info("登陆成功");
                //根据权限，指定返回数据
                String role = userMapper.getRole(username);
                if ("user".equals(role)) {
                    return resultMap.success().code(200).message("欢迎登陆");
                }
                if ("admin".equals(role)) {
                    return resultMap.success().code(200).message("欢迎来到管理员页面");
                }
            }
        } catch (Exception e) {
            log.info("对用户进行登录验证...验证未通过,未知账户");
            resultMap.put("message", e.getMessage());
        }

        return resultMap;
    }
}