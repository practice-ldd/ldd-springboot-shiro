package springcloud.service.shiro.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springcloud.service.shiro.mapper.UserMapper;
import springcloud.service.shiro.model.ResultMap;

/**
 * @Auther: liudong
 * @Date: 18-11-2 15:03
 * @Description:
 */
@RestController
@RequestMapping("/admin")
//@RequiresRoles("admin")//该注解在类上无效
public class AdminController {
    private final ResultMap resultMap;
    private final UserMapper userMapper;
    @Autowired
    public AdminController(UserMapper userMapper, ResultMap resultMap) {
        this.userMapper = userMapper;
        this.resultMap = resultMap;
    }

    @GetMapping(value = "/getMessage")
    public ResultMap getMessage() {
        return resultMap.success().code(200).message("该接口不做角色限制，可以获得该接口的信息！");
    }

    @GetMapping("/getUser")
    @RequiresRoles("admin")
    public ResultMap getUser() {
        List<String> list = userMapper.getUser();
        return resultMap.success().code(200).message(list);
    }
}