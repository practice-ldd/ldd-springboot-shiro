package springcloud.service.shiro;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

@SpringBootApplication
//@EnableEurekaClient
@EnableFeignClients
@EnableHystrixDashboard
@EnableHystrix
@MapperScan(basePackages = {"springcloud.service.shiro.mapper"})
//@ComponentScan(basePackages = {"com.demo2do","com.suidifu", "com.zufangbao"})
public class ShiroServerApplication {

    public static void main(String[] args) {

        SpringApplication.run(ShiroServerApplication.class, args);
        System.out.println("启动完毕");

    }
}
