package springcloud.service.shiro.model;

import java.util.HashMap;
import org.springframework.stereotype.Component;

/**
 * @Auther: liudong
 * @Date: 18-11-2 15:14
 * @Description:
 */
@Component
public class ResultMap extends HashMap<String, Object> {
    public ResultMap() {
    }

    public ResultMap success() {
        this.put("result", "success");
        return this;
    }

    public ResultMap fail() {
        this.put("result", "fail");
        return this;
    }

    public ResultMap message(Object message) {
        this.put("message", message);
        return this;
    }

    public ResultMap code(int code) {
        this.put("code", code);
        return this;
    }
}
